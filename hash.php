<?php
/**
 * Класс хэширования параметров для обеспечения 
 * максимальной конфиденциальности передаваемых данных.
 * 
 * Автор
 * Алексей Никитин
 * Версия 1.0
 * 
 * Версия PHP 7.4
 */

class Hash
{
    protected string $activity; //Вид деятельности
    protected string $date;     //Возраст
    protected string $gender;   //Половой признак
    protected string $income;   //Уровень дохода
    protected string $APIkey;   //Ключ API выдается при регистрации приложения и является солью для хэширования

    public function __construct(
        string $activity, 
        string $date,
        string $gender,
        string $income,
        string $APIkey
    )
    {
        $this->activity = $activity;
        $this->date     = $date;
        $this->gender   = $gender;
        $this->income   = $income;
        $this->APIkey   = $APIkey;
    }

    public function hashData()
    {
        $hashString = $this->activity . $this->date . $this->gender . $this->income . $this->APIkey;
        
        return hash('sha3-512' , $hashString);
    }
}